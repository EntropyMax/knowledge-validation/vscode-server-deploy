# Table of Contents
- [vscode-server-deploy](#vscode-server-deploy)
  - [Definitions](#definitions)
  - [Resource Requirements](#resource-requirements)
  - [Dependencies](#dependencies)
  - [Installation](#installation)
  - [Initial Setup](#initial-setup)
  - [Verification](#verification)
  - [Help Using Scripts](#help-using-scripts)
  - [setup.py](#setuppy)
  - [References](#references)

# vscode-server-deploy

These instructions will help you setup a system that can host VS Code Server instances for multiple examinees. A 
`Python` script was developed to generate a `docker-compose.yaml` file for container mount points.

# Definitions
- Host: The system that will run the VS Code Server instances
- Instance: A virtual machine or docker container

# Resource Requirements
We recommend the following minimum resources.
- Public facing IP (for installations within a cloud environment)
- At least one dual core CPU (or two CPUs in a cloud environment) with eight GB of RAM
- At least 30 GBs of storage (60 GBs for systems hosting more than six users)

# Dependencies
The following software must be installed on the host running the VS Code Server instances.
- [Python 3.7+](https://www.python.org/downloads/) 
- [Docker](https://docs.docker.com/get-docker/)
- [Docker-Compose](https://docs.docker.com/compose/install/)

# Installation
1. Ensure the host has the [Dependencies](#dependencies) installed.
2. Clone [this repo](https://gitlab.com/EntropyMax/knowledge-validation/vscode-server-deploy) onto the machine hosting these 
   services.
3. Change into the cloned directory; the default is `vscode-server-deploy`.
4. Ensure a swap file is configured on the host.
   - If running Ubuntu, or something similar, run `enable_swap.sh` to check, enable and use a swap file.
   - Otherwise, check your OS documentation on setting up a swap (page) file.

# Initial Setup
1. List examinee names and the URL to their exam repository in the `input.txt` file. 
   (**MAY NEED CLARIFICATION FOR EVAL VS. PRACTICE CONFIGURATIONS**).
```
John Doe https://gitlab.com/EntropyMax/knowledge-validation/basic-dev-prac-env.git
Mary Sue https://gitlab.com/EntropyMax/knowledge-validation/basic-dev-prac-env.git
```
2. Run the `setup.py` script.
   - If the installation is on a local system, run `setup.py -i 127.0.0.1` 
     (**MAY WANT TO CONSIDER MAKING THIS THE DEFAULT, THINKING OF THE CONFIGURATION WITH THE MOST USE**).
   - Otherwise, run `setup.py`.
3. Setup the network-server container's server.py.
   - For evaluation configurations, copy the relevant `server.py` file into the `vscode-server-deploy` folder; the 
     cloned version of this repo.
   - (**PROBABLY WANT SOMETHING HERE FOR THE PRACTICE ENVIROMENT, ONCE IT'S AVAILABLE**).
4. Run `docker-compose up -d`.

```sh
./setup.py
docker-compose up -d
```

**NOTE: `git clone` is used for every entry in `input.txt`; we recommend using `git config --global credential.helper
cache` so you only have to input your username and password once.

# Verification
1. Verify all containers are running using `docker-compose ps`.
2. For evaluation configurations, verify the network-server container is running `server.py`. 
   (**THIS WILL EVENTUALLY APPLY TO THE PRACTICE CONFIGURATION AS WELL**)
   - Run `docker ps` to get the container ID.
   - Run `docker container exec -it <container ID> /bin/bash` to get a shell within the container.
   - Verify the `server.py` process is running using `ps -aux`.
3. Verify any applicable custom key bindings function properly using a question from the test bank for testing.

# Help Using Scripts
This information explains how to use each script.

## setup.py
```txt
usage: setup.py [-h] [-f FILEPATH] [-i ADDRESS]

setup script use to stage vscode-server environments

optional arguments:
-h, --help show this help message and exit
-f FILEPATH, --file FILEPATH
specify input file to stage repos and docker-compose.yml file from
(default is input.txt). This file is a list of names to stage each exam.
-i ADDRESS, --ip ADDRESS
specify host ip address to bind containers to (default is 0.0.0.0)
```

# References

- https://docs.docker.com/engine/reference/builder/
- https://docs.docker.com/compose/compose-file/
