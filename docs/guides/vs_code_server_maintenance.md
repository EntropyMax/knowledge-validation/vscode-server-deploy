# Table of Contents
- [Updates](#updates)
- [Docker Image Cleanup](#docker-image-cleanup)
- [Clean Previous Exam Instances](#clean-previous-exam-instances)
- [Setup VS Code Servers for Each Examinee](#setup-vs-code-servers-for-each-examinee)
- [Restrict Access During Lunch](#restrict-access-during-lunch)

# Updates
As the VS Code Server environment evolves, updates will be published to this repository. To use the latest features, 
update your host by performing the following.
- Change into the `vscode-server-deploy` directory; if you used a different name during cloning, you'll need to use 
  that name.
- Use `git pull` to download the latest changes.

# Docker Image Cleanup
After a while, your host will have several older images. To free up space, remove all outdated images. If you want to 
ensure you have the latest image, you can remove all images; the next time you run docker-compose up, the latest image 
will be downloaded automatically. Perform the following commands to remove images.
- To see a list of images on your system: `docker image ls`
- To remove one or more images: `docker image rm <image_ID1> <image_ID2> ...`

# Clean Previous Exam Instances
Prior to using this environment for another exam, ensure any previously run exams are removed by performing the 
following steps.
- Change into the `vscode-server-deploy` directory.
- Remove all examinee based folders
- Within the `vscode-server-deploy/input.txt` file, remove all entries for past examinees
- Use the following commands to remove unused containers, volumes and images.
  - `docker container prune`
  - `docker volume prune`
  - `docker image prune`
- When you're ready to start each container, add the `--remove-orphans` option to the `docker-compose up -d` command. 
  Note, this only needs done once after removing examinee folders.

# Setup VS Code Servers for Each Examinee
After [cleaning previous exams](#clean-previous-exam-instances), follow the [initial setup](vs_code_server_setup.md#initial-setup) 
instructions to setup the environment for another exam and verify the setup using the [verification](vs_code_server_setup.md/#verification) 
instructions.

# Restrict Access During Lunch
The best way to restrict access to each instance on the host is to block the remote access ports at the firewall. This 
approach ensures user data stays intact while preventing remote access during the lunch period. If your host is in a 
cloud environment, this can be accomplished using the cloud environment's security settings of the instance running 
the host. For example, in AWS you would perform the following steps (consult your cloud vendor documentation to 
perform similar procedures): 
- From the `EC2 Dashboard`, click `Instances` in the left menu
- Under the list of Instances that appear in the pane on the right, select the affected instance
- In the details that appear under the Instances list, click the `Security` tab
- Scroll down slightly and find the entry titled `Security groups`
- Click the link to the security group needing modification
- After the page loads, click the `Inbound Rules` tab
- On the far right, click the `Edit Inbound Rules` button
- Find the rule for the applicable ports
- Record the current settings somewhere so you can change it back
- Under the `Source` column, change the drop down so it reads `My IP`
- Now you will be the only one able to access the exam containers using a browser
- After the lunch period is expired, reset the settings to its previous state
