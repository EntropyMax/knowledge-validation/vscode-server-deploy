# Table of Contents
- [Connecting to the VS Code Server Environment](#connecting-to-the-vs-code-server-environment)
- [Setting up SSH for your GitLab Account (Optional)](#setting-up-ssh-for-your-gitlab-account-optional)

# Connecting to the VS Code Server Environment
The information below explains how to access the VS Code Server environment. After connecting to the environment, 
further instructions are available in markdown documents.

## Eval Environment
1. Your exam administrator will provide you with a `[PUBLIC_IP]`, a `[PORT]`, and a `password` for your instance
2. Open your browser (**Must be a Chromium based browser**), and enter `https://[PUBLIC_IP]:[PORT]`
3. If you receive a `your connection is not private` message then allow continuing; the website is configured to use 
   self-signed certs by default.

## Practice Environment
This area is reserved for instructions on connecting to the practice environment.

# Setting up SSH for your GitLab Account (Optional)
If you are using 2 factor authentication, you will not be able to `git clone` using HTTPS unless you use a [personal 
access token](https://gitlab.com/-/profile/personal_access_tokens) with `read_repository` and `write_repository`, or 
you set up SSH. If you do not want to constantly enter your GitLab username and password every time you push, SSH will 
remove that requirement.

1. Follow the [RSA SSH key setup](https://docs.gitlab.com/ee/ssh/README.html#rsa-ssh-keys) using the VSCode-Server 
   terminal (`ctrl+shift+~`), use all default values.
   - During key generation, entering a password is generally more secure; however, if you want to avoid entering a 
     password everytime then do not enter a password.
2. In the same terminal, run `cat ~/.ssh/id_rsa.pub` and copy the output.
3. Paste the SSH key in your [profile keys](https://gitlab.com/-/profile/keys), title it whatever you want, and click 
   `Add key`.
4. Configure git to use the SSH url.
   - If the repo is cloned for you: `git remote set-url origin <ssh-repo-url>`
   - Otherwise, when you clone your repo, use the SSH url instead. Ask your exam administrator for this url if you are 
     using SSH before you start your test.
