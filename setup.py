#!/usr/bin/python3

import os
import random
import string
import argparse
import subprocess

from pathlib import Path

# todo: add check/warning for default values (if overriding params have not been provided) existing before continuing
default_url = 'https://gitlab.com/EntropyMax/knowledge-validation/basic-dev-prac-env.git'
default_keybind_file = ".vscode/keybindings.json"
default_keybind_dst_path = '~/.local/share/code-server/User/'
default_code_server_container = 'registry.gitlab.com/entropymax/knowledge-validation/vscode-server-deploy/vscode-server'

# added for creation of default launch/tasks.json files
# todo: incorporate into parser as added parameters if desired.
# todo: generalize way to add any file directly into the ".vscode" dir
default_dst_path = '~/.local/share/code-server/User/'
default_task_file = ".vscode/tasks.json"
default_task_dst = default_dst_path + 'tasks.json'
default_launch_file = ".vscode/launch.json"
default_launch_dst = default_dst_path + 'launch.json'


def get_git_questions(name: str):
    gitlab_issue_creation_outfile = f'{name}/git_question_1_instructions.md'
    gitlab_issue_creation_url = 'https://gitlab.com/EntropyMax/knowledge-validation/ccd-master-question-file/-/raw/master/performance/Git/gitLab_issue_creation/instructions.md'
    git_cmd_line_outfile = f'{name}/git_question_2_instructions.md'
    git_cmd_line_url = 'https://gitlab.com/EntropyMax/knowledge-validation/ccd-master-question-file/-/raw/master/performance/Git/git_command_line/instructions.md'
    cmds = [
        f'curl -o {git_cmd_line_outfile} {git_cmd_line_url}',
        f'curl -o {gitlab_issue_creation_outfile} {gitlab_issue_creation_url}'
    ]
    for cmd in cmds:
        subprocess.run(cmd.split())


def generate_password():
    '''
    Generate a 24 character password
    '''
    password_length = 24
    possible_characters = string.ascii_letters + string.digits

    random_character_list = [random.choice(possible_characters) for i in range(password_length)]
    random_password = "".join(random_character_list)
    return random_password

def create_config(full_name:str, port_increment:int=0):
    '''
    The config.yaml file in vscode server is used to specify the type of auth, password, if certs are used,
    and the bind addr. This is used to serve inside the container. Do not change the bind-addr expecting
    the container to serve on a different IP. You will need to use the --ip flag for that.
    '''
    with open(f'{full_name}/config.yaml', 'w') as config_file:
        config_file.write((
            f'bind-addr: 0.0.0.0:{8080+port_increment}\n'
            'auth: password\n'
            f'password: {generate_password()}\n'
            'cert: true\n'
        ))

def clone_repo(full_name:str, repo_url:str):
    '''
    Simple subprocess run to clone exam_url in a folder with the persons name
    '''
    cmd = f'git clone {repo_url} {full_name}'
    subprocess.run(cmd.split())

def create_exam_folder(full_name:str):
    '''
    Simple subprocess run to mkdir an exam folder with the persons name
    '''
    cmds = [
        f'mkdir {full_name}',
        f'chmod 777 {full_name}'
    ]
    for cmd in cmds:
        subprocess.run(cmd.split())

def docker_compose_start():
    '''
    The beginning of the docker-compose file. Should only be written once
    '''
    return (
        'version: "3"\n\n'
        'services:\n'
    )

def docker_compose_service(person:str, 
                            ip_addr:str, 
                            keybind_src:str, 
                            keybind_dest:str, 
                            task_src:str, 
                            task_dest:str,
                            launch_src:str, 
                            launch_dest:str,
                            port_increment:int=0, 
                            vscode_server_container:str=default_code_server_container):
    '''
    The services section of the docker-compose.yml. Should be written
    as many times as there are examinees taking the exam
    '''
    name = '-'.join(person.lower().split())
    default_port = 8080
    port = default_port+port_increment
    return (
        f'  {name}:\n'
        f'    image: {vscode_server_container}\n'
        '    restart: always\n'
        '    ports:\n'
        f'      - {ip_addr}:{port}:{port}\n'
        '    volumes:\n'
        f'      - "./{name}:/home/coder/exam"\n'
        f'      - "./{name}/config.yaml:/home/coder/.config/code-server/config.yaml"\n'
        f'      - "./{name}/{keybind_src}:/home/coder/{keybind_dest}"\n'
        f'      - "./{name}/{task_src}:/home/coder/{task_dest}"\n'
        f'      - "./{name}/{launch_src}:/home/coder/{launch_dest}"\n'
        '    entrypoint: >\n'
        '      /bin/bash -c "python3 -m pip install -r requirements.txt;\n'
        # uncomment and modify below if you want to execute a script when the container is spun up
        # '      cd exam;\n'
        # '      python3 createPracticeTest.py;\n'
        # '      rm createPracticeTest.py;\n'
        # '      cd .. ;\n'
        '      /usr/bin/code-server ./exam --user-data-dir=.local/share/code-server;"\n'
        # This is specifically for CCD to give access to a server in the docker network
        '    depends_on:\n'
        '       - network-server\n'
        # Resource limits for examinee containers. reservation = min, limit = max
        '    deploy:\n'
        '      resources:\n'
        '        limits:\n'
        '          cpus: "2"\n'
        '          memory: 1000m\n'
        '        reservations:\n'
        '          memory: 600m\n\n'
    )

def docker_compose_external_services(vscode_server_container:str=default_code_server_container):
    return (
        '  network-server:\n'
        f'    image: {vscode_server_container}\n'
        '    restart: always\n'
        '    ports:\n'
        '      - 5000:5000\n'
        '      - 1337:1337\n'
        '      - 8079:8079\n'
        '    volumes:\n'
        '      - "./server.py:/home/coder/server.py"\n'
        '    entrypoint: >\n'
        '      /bin/bash -c "python3 -m pip install -r requirements.txt;\n'
        '      python3 server.py;"\n'
        # Resource limits for the network-server container. reservation = min, limit = max
        '    deploy:\n'
        '      resources:\n'
        '        limits:\n'
        '          cpus: "2"\n'
        '          memory: 1280m\n'
        '        reservations:\n'
        '          memory: 1024m\n\n'
    )

def parse_input_file(filepath:str) -> list:
    '''
    Parse a simple input file with examinee's name and their exam url. If a line is
    space delimitated, the url should be the last index if split()
    '''
    output = []
    with open(filepath, 'r') as input_file:
        for line in input_file.readlines():
            content = line.split()
            url = content.pop()
            output.append({
                'full_name': '-'.join(content).lower(),
                'exam_url': url
            })
    return output

def create_server_py():
    """
    create a server.py if it doesn't exist
    """
    file_to_create = Path.cwd() / "server.py"
    if file_to_create.is_file():
        print("file exists, no action taken.")
        return "file Exists"
    else:
        file_to_create.touch()
        with file_to_create.open('w') as fh:
            fh.write("""
from http.server import BaseHTTPRequestHandler, HTTPServer
import time

# open an url like http://127.0.0.1/example the method do_GET() i

hostName = "0.0.0.0"
serverPort = 8079

class MyServer(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes("<html><head><title>Sample Webserver</title></head>", "utf-8"))
        self.wfile.write(bytes("<p>Request: %s</p>" % self.path, "utf-8"))
        self.wfile.write(bytes("<body>", "utf-8"))
        self.wfile.write(bytes("<p>This is an example web server, if you see this, the container succeeded.</p>", "utf-8"))
        self.wfile.write(bytes("</body></html>", "utf-8"))

if __name__ == "__main__":        
    webServer = HTTPServer((hostName, serverPort), MyServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
""")

def create_keybindings_file(keybind_file:str):
    """
    if no keybindings.json file exists before provisioning, this will create something
    generic.
    """
    file_to_create = Path.cwd() / keybind_file
    if file_to_create.is_file():
        print("file exists, no action taken.")
        return "file Exists"
    else:
        dir_to_create = file_to_create.parent
        dir_to_create.mkdir(parents=True, exist_ok=True)
        file_to_create.touch(exist_ok=True)
        with file_to_create.open('w') as fh:
            fh.write("""
// Place your key bindings in this file to overwrite the defaults
[]
""")
def create_task_file(the_file:str):
    """
    if no task.json exists before provisioning, this will create something
    generic.
    """
    file_to_create = Path.cwd() / the_file
    if file_to_create.is_file():
        print("file exists, no action taken.")
        return "file Exists"
    else:
        dir_to_create = file_to_create.parent
        dir_to_create.mkdir(parents=True, exist_ok=True)
        file_to_create.touch(exist_ok=True)
        with file_to_create.open('w') as fh:
            fh.write("""{
    "version": "2.0.0",
    "tasks": [
        {
            "label": "Compile C",
            "type": "shell",
            "command": "cmake",
            "args": [
                "--build", "${fileDirname}/build",
                "--config", "Debug",
                "--target", "TestCode",
                "--",
                "-j", "6"
            ],
            "group": {
                "kind": "build",
                "isDefault": true
            },
            "dependsOn": [
                "Build C"
            ]
        },
        {
            "label": "Clean",
            "type": "shell",
            "command": "rm",
            "args": [
                "-r", "${workspaceFolder}/*/*/build", "&&",
                "rm", "${workspaceFolder}/*/*/TestCode"
            ]
        },
        {
            "label": "Build C",
            "type": "shell",
            "command": "cmake",
            "args": [
                "-DCMAKE_C_COMPILER=/usr/bin/gcc",
                "-DCMAKE_CXX_COMPILER=/usr/bin/g++",
                "-DCMAKE_RUNTIME_OUTPUT_DIRECTORY=${fileDirname}",           
                "-B", "${fileDirname}/build",
                "-S", "${fileDirname}"
            ]
        }
    ]
}
""")
def create_launch_file(the_file:str):
    """
    if no launch.json exists before provisioning, this will create something
    generic.
    """
    file_to_create = Path.cwd() / the_file
    if file_to_create.is_file():
        print("file exists, no action taken.")
        return "file Exists"
    else:
        dir_to_create = file_to_create.parent
        dir_to_create.mkdir(parents=True, exist_ok=True)
        file_to_create.touch(exist_ok=True)
        with file_to_create.open('w') as fh:
            fh.write("""
{
    "version": "0.1.0",
    "configurations": [
        {
            "name": "Test C",
            "type": "cppdbg",
            "request": "launch",
            "program": "${fileDirname}/TestCode",
            "cwd": "${fileDirname}",
            "MIMode": "gdb",
            "preLaunchTask": "Compile C"
        },
        {
            "name": "Test Python",
            "type": "python",
            "request": "launch",
            "program": "${fileDirname}/runtests.py",
            "cwd": "${fileDirname}"
        },
        {
            "name": "Test Networking",
            "type": "python",
            "request": "launch",
            "program": "${fileDirname}/runtests.py",
            "cwd": "${fileDirname}"
        }
    ]
}
""")





def main():
    parser = argparse.ArgumentParser(description=f'setup script use to stage vscode-server environments')
    parser.add_argument('-v', '--vscode_server_container', default=default_code_server_container, type=str, metavar='VSCODESERVER',
                        help=f"Specify the destination path for the vscode_server_container location. \
                            The default is '{default_code_server_container}'.")
    parser.add_argument('-f', '--file', default='input.txt', type=str, metavar='FILEPATH',
                        help='specify input file to stage repos and docker-compose.yml file from (default is input.txt). \
                            This file is a list of names to stage each exam.')
    parser.add_argument('-i', '--ip', default='0.0.0.0', type=str, metavar='ADDRESS',
                        help=f'specify host ip address to bind containers to (default is 0.0.0.0)')
    parser.add_argument('--keybindings_file', default=default_keybind_file, type=str,
                        help=f"Specify the location of the keybindings file. The default is '{default_keybind_file}'.")
    parser.add_argument('--keybindings_dst_path', default=default_keybind_dst_path, type=str, 
                        help=f"Specify the destination path for the custom keybindings file. \
                            The default is '{default_keybind_dst_path}'.")
    parsed_args = parser.parse_args()
    
    keybind_file = parsed_args.keybindings_file
    if keybind_file.startswith("~/"):
        keybind_file = "/".join(keybind_file.split("/")[1:])
    
    keybind_dst_path = parsed_args.keybindings_dst_path
    if keybind_dst_path.startswith("~/"):
        keybind_dst_path = "/".join(keybind_dst_path.split("/")[1:])

    keybind_dst_path = os.path.join(keybind_dst_path, os.path.basename(keybind_file))

    vscode_server_container_dst_path = parsed_args.vscode_server_container

    with open('docker-compose.yml', 'w') as dc_file:
        dc_file.write(docker_compose_start())
        port_increment = 0
        for person in parse_input_file(parsed_args.file):
            dc_file.write(docker_compose_service(
                person['full_name'], 
                parsed_args.ip, 
                keybind_file,
                keybind_dst_path, 
                default_task_file,
                default_task_dst,
                default_launch_file, 
                default_launch_dst,
                port_increment,
                vscode_server_container_dst_path
            ))
            clone_repo(person['full_name'], person['exam_url'])
            create_exam_folder(person['full_name'])
            create_config(person['full_name'], port_increment)
            create_keybindings_file(f"{person['full_name']}/{keybind_file}")
            create_task_file(f"{person['full_name']}/{default_task_file}")
            create_launch_file(f"{person['full_name']}/{default_launch_file}")
            port_increment += 1
        dc_file.write(docker_compose_external_services(vscode_server_container_dst_path))
    create_server_py()


if __name__ == "__main__":
    main()
