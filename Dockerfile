FROM codercom/code-server:4.97.2

USER root
# Added glibc-source so examinees are not presented with `unable to find file` when stepping into libraries
RUN apt-get update -y && apt-get install -y python3-dev python3-pip gcc gdb git glibc-source python3-venv zlib1g-dev
# Extract glibc-source files from/to /usr/src/glibc/ so they're available to the debugger
RUN tar xvf /usr/src/glibc/glibc-2.36.tar.xz -C /usr/src/glibc/

ARG USERNAME=coder
ARG HOME=/home/$USERNAME
ARG CODE_KEYBIND_DIR=$HOME/.local/share/code-server/User
ARG CONFIG_DIR=$HOME/.config/code-server

USER $USERNAME
WORKDIR $HOME

# Copy VS Code extentions for cpptools and Python.
# These files were included in the repo to avoid potential download outages due to Microsoft's threshold being exceeded.
COPY ms-vscode.cpptools-1.1.3.vsix .
COPY ms-python.python-2020.10.332292344.vsix .
COPY bierner.markdown-mermaid-1.9.2.vsix .
# Now install the extensions into VS Code Server
# use `|| true` b/c there's an issue with code-server sending back 500 server errors even though the extension installs correctly
RUN code-server --install-extension ms-vscode.cpptools-1.1.3.vsix || true
RUN code-server --install-extension ms-python.python-2020.10.332292344.vsix || true
RUN code-server --install-extension bierner.markdown-mermaid-1.9.2.vsix || true

# Disabled download, uncompress & install due to the potential for the links to be blocked because of exceeding Microsoft's threshold
# We are keeping the code in case we want to go back to this in the future
# Install specific packages
#RUN curl https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-vscode/vsextensions/cpptools/1.1.3/vspackage -o ms-tools.cpptools.gz && \
#    gunzip -c ms-tools.cpptools.gz > ms-tools.cpptools.vsix && \
    # use `|| true` b/c there's an issue with code-server sending back 500 server errors even though the extension installs correctly
#    code-server --install-extension ms-tools.cpptools.vsix || true
#RUN curl https://marketplace.visualstudio.com/_apis/public/gallery/publishers/ms-python/vsextensions/python/2020.10.332292344/vspackage -o ms-tools.python.gz && \
#    gunzip -c ms-tools.python.gz > ms-tools.python.vsix && \
#    code-server --install-extension ms-tools.python.vsix || true

COPY requirements.txt .
RUN if [ -f "requirements.txt" ]; then pip3 install --break-system-packages --user -r requirements.txt; fi
ENV PATH="/home/coder/.local/bin:$PATH"
COPY deploy-settings.json .local/share/code-server/User/settings.json
# Add files that will be volume mounted so they don't become folders due to not existing
# This is the config file for VS Code Server
RUN if [ ! -d "${CONFIG_DIR}}" ]; then mkdir -p ${CONFIG_DIR}; fi
RUN if [ ! -f "${CONFIG_DIR}/config.yml" ]; then touch "${CONFIG_DIR}/config.yml"; fi
# This is the keybindings settings for VS Code Server
RUN if [ ! -d "${CODE_KEYBIND_DIR}" ]; then mkdir -p ${CODE_KEYBIND_DIR}; fi
RUN if [ ! -f "${CODE_KEYBIND_DIR}/keybindings.json" ]; then touch "${CODE_KEYBIND_DIR}/keybindings.json"; fi

# Ensure coder has access to the directories and files within its home directory
RUN sudo chown -Rh ${USER}:${USER} ${HOME}

ENTRYPOINT ["dumb-init", "fixuid", "-q", "/usr/bin/code-server", "./exam", "--user-data-dir=.local/share/code-server"]
